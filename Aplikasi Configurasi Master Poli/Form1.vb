﻿Imports Newtonsoft.Json.Linq
Imports System.IO
Imports Newtonsoft.Json

Public Class Form1
    Dim isExist As Boolean
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            Dim configText As String = clsConfig.Load()

            ' Tampung pada object json
            Dim jsonSettings As JObject = JObject.Parse(configText.Replace(Chr(10), Chr(13)))

            ' Simpan nilai file config ke variable property
            My.Settings.Item("ConnectionString") = String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=False;User ID=sa;Password=S3cr3t", jsonSettings("DB")("Host").ToString, jsonSettings("DB")("Name").ToString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub New", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End
        End Try
    End Sub
    Dim tes As New dsTableAdapters.M_POLITableAdapter
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            tes.Fill(Ds.M_POLI)

            Dim dt As DataTable = New ds.TEMP_POLIDataTable

            Dim configText As String = clsPoliConfig.Load()

            ' Tampung pada object json
            Dim jsonSettings As JObject = JObject.Parse(configText.Replace(Chr(10), Chr(13)))

            Dim token As JToken = jsonSettings.GetValue("PoliConfigurations")

            dt = JsonConvert.DeserializeObject(Of DataTable)(token.ToString())

            For Each row In Ds.M_POLI
                Dim dr() As DataRow
                Try
                    dr = dt.Select(String.Format("IDPOLI='{0}'", row.IDPOLI))
                Catch ex As Exception
                End Try
                Dim display As String = ""
                Try
                    display = dr(0)("DISPLAY")
                Catch ex As Exception
                End Try
                Dim prefix As String = ""
                Try
                    prefix = dr(0)("PREFIX")
                Catch ex As Exception
                End Try
                Dim button As String = ""
                Try
                    button = dr(0)("CONTROLBUTTON")
                Catch ex As Exception
                End Try
                Dim label As String = ""
                Try
                    label = dr(0)("CONTROLDISPLAY")
                Catch ex As Exception
                End Try

                Ds.TEMP_POLI.NewRow()
                Ds.TEMP_POLI.Rows.Add(row.IDPOLI, display, prefix, button, label)
            Next
            Ds.TEMP_POLI.AcceptChanges()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Load", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End
        End Try
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        Try
            Dim configText As String = clsPoliConfig.Load()
            Dim jsonSettings As JObject = Newtonsoft.Json.JsonConvert.DeserializeObject(configText)

            If Not IsDBNull(jsonSettings.Property("PoliConfigurations")) Or Not IsNothing(jsonSettings.Property("PoliConfigurations")) Then
                jsonSettings.Property("PoliConfigurations").Remove()
            End If

            ' Convert dari datatable ke string
            Dim inputJson = JsonConvert.SerializeObject(Ds.TEMP_POLI)

            ' Convert dari string ke JArray
            Dim jArrObject = JArray.Parse(inputJson)

            ' Tambahkan Item PoliConfigurations dengan value JArray
            jsonSettings.Add("PoliConfigurations", jArrObject)

            Dim textFinal = Newtonsoft.Json.JsonConvert.SerializeObject(jsonSettings, Newtonsoft.Json.Formatting.Indented)

            File.WriteAllText(clsPoliConfig.GetConfigFilePath, textFinal)

            MessageBox.Show("Save config successful.", "Save Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Save", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
