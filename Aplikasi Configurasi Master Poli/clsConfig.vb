﻿Imports System.IO

Public Class clsConfig
    Public Shared DefaultConfig = "{'DB':{'Host':'','Name':''}}"

    Public Shared ReadOnly Property GetConfigFilePath() As String
        Get
            Return String.Format("{0}\{1}", Application.StartupPath(), "config.ini")
        End Get
    End Property
    Public Shared Function Load() As String
        Try
            Return File.ReadAllText(GetConfigFilePath)
        Catch ex As Exception
            Return DefaultConfig.Replace("'", """")
        End Try
    End Function
End Class

Public Class clsPoliConfig
    Public Shared DefaultConfig = "{'PoliConfigurations':[]}"

    Public Shared ReadOnly Property GetConfigFilePath() As String
        Get
            Return String.Format("{0}\{1}", Application.StartupPath(), "poli_config.ini")
        End Get
    End Property
    Public Shared Function Load() As String
        Try
            Return File.ReadAllText(GetConfigFilePath)
        Catch ex As Exception
            Return DefaultConfig.Replace("'", """")
        End Try
    End Function
End Class
